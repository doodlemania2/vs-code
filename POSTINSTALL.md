#Single User
The password stored in /app/data/config.yaml will reset on each restart of the app. If you need more than a single user, you should install multiple instances.

#Extension Marketplace
The extensions are fixed at installation due to a licensing scenario. You can acquire new extensions and install them via instructions here: https://github.com/cdr/code-server/blob/master/doc/FAQ.md#how-can-i-request-a-missing-extension. 

From the Coder extension marketplace
code-server --install-extension ms-python.python

From a downloaded VSIX on the file system
code-server --install-extension downloaded-ms-python.python.vsix

Please do not use the Microsoft extension marketplace as that would be a violation of the Microsoft license.
