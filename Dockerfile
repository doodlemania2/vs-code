FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

# Install Basic Dependencies
RUN apt-get update && apt-get upgrade -y
RUN apt install jq apt-transport-https -y
RUN mkdir -p /app/data/global /app/code
## For whatever reason, the full home dir symlink doesnt work if the folder exists, lets remove and link it :)
RUN rm -rf /home/cloudron
RUN ln -s /app/data/global /home/cloudron
RUN chown -R cloudron:cloudron /home/cloudron

# Install Mono-Complete for .NET development
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
RUN echo "deb https://download.mono-project.com/repo/ubuntu stable-bionic main" | sudo tee /etc/apt/sources.list.d/mono-official-stable.list
RUN apt update
RUN apt install -y mono-complete

# Install PowerShell Core
RUN cd /app/code && \
	wget https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O /app/code/powershell.deb && \
	dpkg -i /app/code/powershell.deb && \
	apt update && \
	apt install -y powershell dotnet-sdk-3.1 -y

# get code-server pacakges
RUN cd /app/code && \
        wget https://github.com/cdr/code-server/releases/download/v3.4.1/code-server-3.4.1-linux-amd64.tar.gz -O /app/code/codeserver.tar.gz -nv && \
        tar xvzf codeserver.tar.gz && \
        mv ./code-server-3.4.1-linux-amd64 /app/code/code-server

# Set permissions
RUN chown -R cloudron:cloudron /app/data
# Setup dir's and binary for code-server
ADD start.sh /app/code/start.sh
RUN mkdir -p /app/data/workdir
RUN chmod +x /app/code/start.sh

# Run start script
CMD [ "/app/code/start.sh" ]
